# Exercise 1 - Install The Debian Virtual Enviroment

[Get the virtual machine partition from this link](https://www.osboxes.org/debian/#debian-9-vmware)

Choose Debian 9.5 64bit for VMWare.
With the file downloaded follow this guide to install nginx on it.

[Installation guide to NGINX](https://www.rosehosting.com/blog/how-to-install-and-configure-nginx-on-debian-9/)

After the installation done and tested the nginx is working fine. Download Nikolai's example website and configure the NGINX to work
with the IP address that we got from Morten.

### Debian installation 

Create a virtual machine and select "install operating system later". After that delete the hard drive of the virtual machine and add a new one
where use the "use an existing hard drive" and select the downloaded Debian virtual machine.

![Debian Install hard drive](Capture.PNG)

![Debian Install existing hard drive](Capture2.PNG)
