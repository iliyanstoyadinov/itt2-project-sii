# Exercise 2 - Temperature in celsius

Convert the ADC values to temperatire in celsius.

### Math implemented in the function:

![picture 1, The function](Function.PNG)

We are using 5V and analog input 1 on the Arduino(A1). 
First we need to find the voltage, so we multiply the ADC output with 5(because we use 5 volts) and after that we divide by 1024 to find the step.
For the clesius we substract 0.5 from teh voltage and then we multiply by 100.

### Results:

The first results are showing the room temperature - 20 degrees celsius. 
The last one shows the temperature after lighter was sued near the sensror - 29 degrees.

![picture 1, Results](temp.PNG)



