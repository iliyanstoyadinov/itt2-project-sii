from flask import Flask, request
from flask_restful import reqparse, abort, Api, Resource
from flask_cors import CORS, cross_origin
import serial
import time


serial_cfg = {  "dev": "/dev/ttyS0",
                "baud": 9600 }

app = Flask(__name__)
CORS(app, resources=r'/*') # to allow external resources to fetch data
api = Api(app)

@api.resource("/")
class url_index( Resource ):
    def get( self ):
        with serial.Serial(serial_cfg['dev'], serial_cfg['baud'], timeout=1) as ser:
            try:
                ser.write("getadcval\n".encode())
                reply = ser.readline().decode()
                returnMessage = ( "- {}".format(reply))
                return returnMessage
            except:
                pass

if __name__ == '__main__':
    app.run(host="0.0.0.0", debug=True)
