# Exercise 1 - NGINX setup and results

Installation of NGINX and how to confiugre it.

### Installation:

For the installation and cnfiguration we just used this [Website](https://www.rosehosting.com/blog/how-to-install-and-configure-nginx-on-debian-9/),
follow the instructions and it should be easy to set up.

After the installation and configuration run the following commands to get all the neccesary settings ready to use flask. 

![restex 1, The setup](restex1.PNG)


### Results:

After everything has been setup and configured, when you run the flask this is waht you should see and you can access it from the same network
through a web browser.

![restex 2, Results](restex2.PNG)



