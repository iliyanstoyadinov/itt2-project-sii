---
title: 19S ITT2 Project Group SII
subtitle: Project plan
authors: ['Iliyan Stoyadinov iliy0031@edu.eal.dk', 'Ivan Poev ivan3026@edu.eal.dk', 'Soma Gergo gerg0127@edu.eal.dk', 'Martynas Mikailionis mart59p9@edu.eal.dk']
date: \today
email: ['iliy0031@edu.eal.dk', 'ivan3026@edu.eal.dk', 'gerg0127@edu.eal.dk', 'mart59p9@edu.eal.dk']
left-header: \today
right-header: Project plan
skip-toc: false
---



# Background

This is the semester project for ITT2 where we will combine different technologies to go from sensor to cloud. This project will cover the first part of the project, and will match topics that are taught in parallel classes.


# Purpose


The main goal is to have a system where sensor data is send to a cloud server, presented and collected, and data and control signal may be send back also. This is a leaning project and the primary objective is for the students to get a good understanding of the challenges involved in making such a system and to give them hands-on experience with the implementation also.

For simplicity, the goals stated wil evolve around the technical project goals. It must be read using the weekly plan as a complementary source for the *learning* oriented goals.


# Goals

The overall system that is going to be build looks as follows

![project_overview](project_overview.PNG)

Reading from the left to the right
* Analog input/output and digital input/output: These are sensor of different kinds that we are to implement into the system. Initially we will work with a temperature sensor.
* ATMega328: The embedded system to run the sensor software and to be the interface to the seralconnection to the raspberry pi.
* Raspberry Pi: Minimal linux system relevant programs to upload/download data from the ATMega and to/from the APIs.
* Juniper router: Router to protect your internal system from the untrusted networks, and to enable access to the Internet and the "cloud servers"
* Reverse proxy/API gateway: This is a server/router that collects and protects the API endpoints and webservers implemented by each group.
* webservers: There are one webserver per group. It will include the REST API implementation and the user interface website.


Project deliveries are:
* A system reading and writing data to and from the sensors/actuators
* A REST API exposing the values of the sensors and option of setting and applying the actuator values.
* A secure firewall enabling connections to the API.
* Docmentation of all parts of the solution
* Regular meeting with project stakeholders.
* Final evaluation

The project itself will be divided into 3 phases and gitlab will be used as the project management platform.


# Schedule

The project is divided into three phases from now to the easter holidays.

See the lecture plan for details.


# Organization

## Group:
*  Iliyan Stoyadinov iliy0031@edu.eal.dk @iliyanstoyadinov
*  Ivan Poev ivan3026@edu.eal.dk @ivan3026 
*  Soma Gergo Gallai gerg0127@edu.eal.dk @GallaiSoma
*  Martynas Mikailionis mart59p9@edu.eal.dk @Martynas13

Tasks not specified yet.


# Budget and resources

No monetary resources are expected. In terms of manpower, only the people in the project group are expected to contrbute,


# Risk assessment

## The most important risks we have to look out is:
*  Time Management
*  Poor communication
*  Too much ambition
*  Electrical failiure
*  Lack of knowledge

## How to overcome these risks:
#### Time management:
* We need to make group meeting, so we are on the same page.
* Never leave issues unresolved. 
* Do only one task at a time.


#### Communication:
* Clear tasks, so everyone is aware what to do and what is the deadline.
* Show up on the meetings - with the group or teachers.
* Milestones.
* Make SMART tasks.


#### Too much ambition:
* Do not waste time on only one task to make it perfect. Better finish all tasks first and then start to improve everything.
* We need to have well defined minimum and to adjust goal(limited output).


#### Electrical failiure:

* Have backup. 


#### Knowledge:
* Seek knowledge or ask someone in the group to help you. 
* Every group member should be honest with the goals and never hide his progress. 
* We can reduce the scope of the task, if it is necessary. 



# Stakeholders

Stakeholders are people who have direct contact with the project.

Internal Stakeholders:
* The group
* Lecturers

Extrnal Stakeholders:
* End users 


# Communication

* Weekly meetings in person with the lecturers.
* Atleast two times per week meeting with the group to work on the project.
* The group has a facebook group to help eachother in the project work, when we can't meet in person.
* At the end of week 13, the end users will test our product and it is going to be evaluated.


# Perspectives

This project will serve as a template for other similar project, ike the second one in ITT2 or in 3rd semester.

It can also serve as an example of the students abilities in their portfolio, when applying for jobs or internships.


# Evaluation

The evaluation will be made at the end of week 13 with our end users. They will test the project and we will evaluate the project depending on their opinion.


# References

None at this time.
