## Initialize port

initPort(DDRB, 0b00110000); - output for two LEDs


## The function 


	void statusBlink(int blink, int period)
	{
		while(blink > 0)
		{
			
			if(period == 25)
			{
				PORTB |= 0b00010000;
				_delay_ms(250);
				PORTB &=~ 0b00010000;
				_delay_ms(250);
			}
			if(period == 5)
			{
				PORTB |= 0b00010000;
				_delay_ms(500);
				PORTB &=~ 0b00010000;
				_delay_ms(500);
			}
			if(period == 1)
			{
				PORTB |= 0b00010000;
				_delay_ms(1000);
				PORTB &=~ 0b00010000;
				_delay_ms(1000);
			}

			blink = blink - 1;
		}
	} 

## Implementation
* We use statusBlink(blink,period);
* "blink" depends on the status_protocol.md file.
* For "period" - 1 is 1 sec; 5 is 0.5 secs; 25 is 0.25secs. 
* ![part 1 implementation](implementation1.PNG)
* ![part 2 implementation](implementation2.PNG)
