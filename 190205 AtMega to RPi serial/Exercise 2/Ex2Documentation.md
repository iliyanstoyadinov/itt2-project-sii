# Exercise 2 Documentation

1. We are using the Arduino development board so we choosed the Arduino Branch.
2. The flowchart of the code was made in Draw.io. 
![picture 1, Flowchart](Flowchart_1.PNG)
3. We are using 5V and GND from Arduino, for the button we are using pin12 and pin8 for the LED.
4. Via UART we can send the following commands:
 * led1on (turns on led1, replies with led1=1)
 * led1off (turns off led1, replies with led1=0)
 * getbtn1 (reads button 1 status, replies with btn1=0 or btn=1, resets status if btn1=1)
 * getadcval (returns raw adc value from ADC)
5. By using the guide - "Arduino Uno Atmel Studio guide" we downloaded the code to the development board.
6. The serial connection between our computer and Arduino board through Putty. 
![picture 2](putty.png)
7. The commands and responses to the ATmega328
![picture 3](commands.png)  
