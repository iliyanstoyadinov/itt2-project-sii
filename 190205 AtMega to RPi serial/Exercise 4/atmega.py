#!/usr/bin/env python3

import serial
import time
import os

def menu():
    print("1. Turn on LED")
    print("2. Turn off LED")
    print("3. Get state of button")
    print("4. Exit")

loop = True

serial_cfg = {  "dev": "/dev/ttyS0",
                "baud": 9600 }

if __name__ == "__main__":
    print( "Running port {}".format( serial_cfg['dev'] ) )

    with serial.Serial(serial_cfg['dev'], serial_cfg['baud'], timeout=1) as ser:
        try:
            while loop:
                os.system('clear')
                menu()
                option = input("Enter your choice: ")
                if option == 1:
                    print("led1on")
                    ser.write( "led1on\n".encode() )
                    reply = ser.readline().decode()   # read a '\n' terminated line
                    print("- {}".format(reply))
                if option == 2:
                    print("led1off")
                    ser.write( "led1off\n".encode() )
                    reply = ser.readline().decode()   # read a '\n' terminated line
                    print( "- {}".format(reply))
                if option == 3:
                    print("getbtn1")
                    ser.write("getbtn1\n".encode())
                    reply = ser.readline().decode()
                    print( "- {}".format(reply))
                if option == 4:
                    loop = False
                if (option != 1 and option !=2 and option != 3 and option != 4):
                    print("wrong input, try again")
		time.sleep(5)
        except KeyboardInterrupt:
            print('interrupted!')

