# Exercise 1 - Documentation


![picture 1, The circuit](190129_picture_of_the_circuit.jpg)


### LED test:

The LED has been soldered to the circuit(see picture 1).
The circuit has been powered with +5V and the LED lit up, proving the connection is correct in the circuit.
We are using 330ohm resistance in the circuit to get the desired voltage.


### Button test:
 
 The button has been soldered to the circuit(see picture 1).
 The circuit has been powered with +5V and when we push the button the multimeter shows 0V(ground).

 
### Temperature sensor test:
 
The temperature sensor has been soldered to the circuit(see picture 1).
The circuit has been powered with +5V and we measured on the Vout pin on the sensor.
The result we got was 0.76V and compared to the datasheet it is around 24-25°C(see picture 2).

![picture 2, Multimeter results - TMP36](190129_picture_of_the_multimeter_with_TMP36.jpg)