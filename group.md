Group: SII
-----------------------------------------

Class: A

Members:

* Iliyan Stoyadinov @iliyanstoyadinov
* Ivan Poev @ivan3026
* Soma Gergo Gallai @GallaiSoma
* Martynas Mikailionis @Martynas13

Juniper router:

* Management SSH: 10.217.19.215
* External ip: 10.217.19.215/22

Raspberry

* SSH access: 10.10.2.2:18002
* REST API access: 10.217.19.215:18002

Group web server:

* SSH access: <ip>:<port>
* REST API access: 10.217.16.165:80
