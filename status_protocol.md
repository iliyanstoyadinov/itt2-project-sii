## LED Status

One blink - ADC value;
Two blinks - LED;
Three blinks - button;
Five blinks - Error!!!

* For ADC value - Blink one time with 2s (1s on, 1s off)
* For LED on - Blink two time with 1s period (0.5s on, 0.5s off)
* For LED off - Blink two time with 2s period (1s on, 1s off)
* For button pressed - Blink three with 1s (0.5s on, 0.5s off)
* For button not pressed - Blink three times with 2s (1s on, 1 off)
* For Error - Blink 5 times with 0.5s period (0.25s on, 0.25s off)